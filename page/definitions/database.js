const { MongoClient } = require('mongodb');
const url = "mongodb://root:example@mongo"

const option = {};

MongoClient.connect(url, option, function(err, client){
  if(err !== undefined){
    console.log(err);
    client.close();
    return;
  }

  DEF.db = client.db('pokefront');
})
