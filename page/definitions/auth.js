const {ObjectId} = require('mongodb');

let opt = {};
opt.secret = CONF.cookie_secret;
opt.cookie = CONF.cookie;

opt.onread = function (meta, next) {

  // meta.sessionid {String}
  // meta.userid {String}
  // meta.ua {String} A user-agent
  // next(err, USER_DATA) {Function} A callback function
  console.log(meta);

  DEF.db.collection('users').findOne(
    {_id: ObjectId(meta.userid)}
  ).then(user => user && next(null, user))
    .catch(err => next(err, null));
};

opt.onfree = function (meta) {}

AUTH(opt);

MAIN.session = opt;
