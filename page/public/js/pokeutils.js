/* 
 * This script realies on importing pokeapi-js-wrapper
 * in the view
 */

/* 
 * pokemon gen ranges since pokeapi makes my life harder
 * for no real good reason and excludes evolutions
 * from the list of pokemon in a gen when querying the
 * gen directly
 *
 * numbers are pokemon id - 1
 */

const genRange = {
  1: [0, 150],
  2: [151, 250],
  3: [251, 385],
  4: [386, 492],
  5: [493, 648],
  6: [649, 720],
  7: [721, 808],
  8: [809, 904]
};

// also a mapper from genName to genId will be usefull

const genNameToId = {
  "generation-i": 1,
  "generation-ii": 2,
  "generation-iii": 3,
  "generation-iv": 4,
  "generation-v": 5,
  "generation-vi": 6,
  "generation-vii": 7,
  "generation-viii": 8
};

const genIdToName = {
  1: "generation-i",
  2: "generation-ii",
  3: "generation-iii",
  4: "generation-iv",
  5: "generation-v",
  6: "generation-vi",
  7: "generation-vii",
  8: "generation-viii",
};

// and another object to transform gen into a set of games
// for move retrieval
const genGamesInGen = {
  "generation-i": ["red-blue", "yellow"],
  "generation-ii": ["gold-silver", "crystal"],
  "generation-iii": ["ruby-sapphire", "emerald", "firered-leafgreen", "colosseum", "xd"],
  "generation-iv": ["diamond-pearl", "platinum", "heartgold-soulsilver"],
  "generation-v": ["black-white", "black-2-white-2"],
  "generation-vi": ["x-y", "omegaruby-alphasapphire"],
  "generation-vii": ["sun-moon", "ultra-sun-ultra-moon", "lets-go-pikachu-lets-go-eevee"],
  "generation-viii": ["sword-shield", "the-isle-of-armor", "the-crown-tundra", "brilliant-diamond-shining-pearl", "legends-arceus"]
};

const P = new Pokedex.Pokedex();

class PokemonInfo {
  constructor(info) {
    this._info = info;
    this.id = this._info.id;
    // this is info that will never change, so might as well add it here now
    this.name = this._info.name;
    this.default_sprite = this._info.sprites.front_default;
    // this is a bit more usefull to have
    this.sprites = this._info.sprites.versions;
    this.types = this._info.types;

    this.stats = {};
    this._info.stats.forEach(data => {
      this.stats[data.stat.name] = data.base_stat;
    });

    // don't really care about detailed move info for now
    //
    // move info comes as a list of moves followed by details of in what game the move isle
    // learned and which manner, but for my intended purpose of displaying moves by gen
    // I have to reorder this
    this._movesByGame = {};
    this._info.moves.forEach(move => {
      move.version_group_details.forEach(group => {
        const game = group.version_group.name;
        let movesArr = this._movesByGame[game] ?? [];
        movesArr.push({
          name: move.move.name,  //lol
          method: group.move_learn_method.name,
          level: group.level_learned_at
        });
        this._movesByGame[game] = movesArr;
      })
    });

    // then we sort since sorted by level is more pleasing
    for (let game in this._movesByGame) {
      this._movesByGame[game].sort((x, y) => x.level - y.level);
    }

    // this variable is indeed meant to change
    // and yes there's no data indicating 
    // the gen the pokemon first appeared so I have to do this

    for (let k in genRange) {
      if (this.id >= genRange[k][0]) {
        // i'm predicting saving the string will be more usefull for querying
        this._gen = genIdToName[k];
        // but this one is used to constrain the minimum generation a pokemon
        // can have, so this one might be more usefull as a number
        this._minGen = parseInt(k);
      }
    };
  }

  set gen(newgen) {
    if (typeof newgen === "number") {
      let newID = Math.max(this._minGen, Math.min(8, newgen));
      this._gen = genIdToName[newID];
    } else {
      this._gen = newgen in genNameToId ? newgen : this._gen;
    }
  }

  get gen() {
    return this._gen;
  }

  get moves() {
    let res = {};
    genGamesInGen[this._gen].forEach(game => {
      res[game] = this._movesByGame[game];
    });
    return res;
  }

  get minGen() {
    return this._minGen;
  }
}

async function pokemonInGen(genNumber) {
  let start = genRange[genNumber][0];
  let end = genRange[genNumber][1];
  let pokemon = await P.resource(`/api/v2/pokemon/?offset=${start}&limit=${end - start}`);
  return pokemon.results;
}

async function pkSmallCardInfo({name}) {
  let {sprites, id} = await P.getPokemonByName(name);
  let sprite = sprites.front_default;
  return {name: name.split("-")[0], sprite: sprite, id: id};
}

async function genCardsInfo(genNumber) {
  return await pokemonInGen(genNumber)
    .then(arr => Promise.all(arr.map(pkSmallCardInfo)));
}

function capitalize(string) {
  return string[0].toUpperCase() + string.substring(1);
}

// tranforms a gen name into a display name
function genNameToDisplayName(genName) {
  // example: generation-iv -> Generation IV
  let [gen, genNum] = genName.split("-");
  return `${capitalize(gen)} ${genNum.toUpperCase()}`
}

// same idea as above for game names
function gameNameToDisplayName(gameName) {
  return gameName.split('-').map(capitalize).join(' ');
}

// same idea as above for sprite names
function spriteNameToDisplayName(gameName) {
  return gameName.split('_').map(capitalize).join(' ');
}

function makeGenDropdownButtons({minGen}) {
  let html = "";
  for (let i = minGen; i <= 8; ++i)
    // i'm relying on changeGen to be defined on the view through a script tag!
    html += `<button class="dropdown-item" type="button" onclick="changeGen(${i})">Gen ${i}</button>\n`;
  return html;
}

function makeGenCard({name, sprite, id}) {
  return `
    <div class="text-capitalize card rounded-lg">
      <a href="/pokemon/${name}">
      <img src="${sprite}" class="card-img-top" alt="${name}">
       <div class="card-body">
          <h5 class="text-muted">#${id.toString().padStart(3, '0')}<br>${name}</h5>
        </div>
      </a>
    </div>
    `;
}

function genSpriteCard(spriteName, sprite) {
  return `
    <div class="align-items-center card rounded-lg sprite-card">
      <img src=${sprite[spriteName]} class="card-img-top" alt=${spriteName}>
      <div class="card-body">
        <h5>${spriteNameToDisplayName(spriteName)}</h5>
      </div>
    </div>
  `;
}

function generateGenSpriteRow(gameName, sprites) {
  //add the name of the gen
  const gameSprites = sprites[gameName];
  let html = `
    <div class="row text-center">
      <div class="col">
        <h5>${gameNameToDisplayName(gameName)} Sprites </h5>
      </div>
    </div>`;
  // for sprite, add a column that contains the sprite card
  html += '<div class="row">';
  for (let spriteName in gameSprites) {
    // we don't support animated images for now
    // also if a pokemon doesn't have a specific sprite, skip
    if (spriteName === 'animated' || gameSprites[spriteName] === null) continue;
    html += `
      <div class="col">
        ${genSpriteCard(spriteName, sprites[gameName])}
      </div>`;
  }
  html += "</div>";
  return html;
}

function generateSpriteCards({sprites, gen}) {
  const currentSprites = sprites[gen];
  //add the name of the gen
  let html = `
    <div class="row text-center">
      <div class="col">
        <h3>${genNameToDisplayName(gen)}</h3>
      </div>
    </div>`;
  // for each game, add a row for each game with
  // the corresponding sprites for the game
  for (let game in currentSprites) {
    html += generateGenSpriteRow(game, currentSprites);
  }
  return html;
};

function generateMoveListElem(move) {
  let html = `<li class=list-group-item d-flex>`;
  //little bit of recycling functions here
  html += `
    <h5>${gameNameToDisplayName(move.name)}</h5> 
    <p class="text-muted"> Level ${move.level}
    <br>`;
  html += `
    <p class="text-muted">Learn Method: ${move.method}</p>`;
  html += '</li>';
  return html;
}

function generateMoveTable(moves) {
  let html = `
    <ul class="list-group">
    `;

  moves.forEach(move => {
    html += generateMoveListElem(move);
  });

  html += '</ul>';
  return html;
}

function generateMoveTables({moves}) {
  let html = ``;
  for (let game in moves) {
    if (moves[game] === undefined) continue;
    html += `<div class="col-sm-auto text-center">`;
    html += `<h5>${gameNameToDisplayName(game)} Moves </h5>`;
    html += generateMoveTable(moves[game]);
    html += '</div>';
  }
  return html;
}
