/* just semi random values for now */

exports.install = function () {
  NEWSCHEMA('Users', function (schema) {
    schema.define('username', 'String(16)', true);
    schema.define('email', 'Email', false);
    schema.define('password', 'String', (value, model) => value.length >= 8 && value.length <= 32);
    schema.define('role', 'String(10)', false);

    schema.setInsert(function ($, model) {
      if (model.email === undefined){
        $.invalid('401','No email provided');
        return;
      } else if ( model.role === undefined ) {
        $.invalid('500', 'No role');
        return;
      } else if ( !model.email.isEmail()) {
        $.invalid('401', 'Invalid email');
        return;
      }

      let userdb = DEF.db.collection('users');
      userdb.findOne({
        $or: [
          {username: model.username},
          {email: model.email}
        ]
      }).then(user => {
        if (user !== null) {
          if (model.username === user.username){
            $.invalid('401', 'User exists');
          } else {
            $.invalid('401', 'Email already has an account asociated');
          }
          return;
        }
        let salt = U.random_string(10);
        let hashed_password = `sha256\$${salt}\$${model.password.sha256(salt)}`
        userdb.insertOne({
          username: model.username,
          email: model.email,
          password: hashed_password,
          role: model.role
        }).then(() => {
          $.success();
        }).catch(err => {
          console.log(err);
          $.invalid('500');
        });
      })
    });

    schema.addWorkflow('login', function ($) {
      const db = DEF.db;
      const {username, password} = $.body;
      db.collection('users').findOne({
        username: username
      }).then(user => {
        if(user === null){
          $.invalid('401');
          return;
        }
        let [_, salt, hash] = user.password.split('$');
        if (password.sha256(salt) === hash) {
          MAIN.session.authcookie($, UID(), user._id.toString(), '2 days');
          $.audit(`${user._id}:${user.username}`);
          $.success();
        } else {
          $.invalid('401');
        }
      });
    });
    schema.encrypt();
  });
}
