exports.install = function(){
  ROUTE('GET /', genSelection);
  ROUTE('GET /gen', genSelection);
  ROUTE('GET /user/register', 'register');
  ROUTE('GET /pokemon/{pokemon}', viewPokemon);
  ROUTE('POST /user/register', addUser);
}

function viewPokemon(pokemon){
  this.view('index', { viewPokemon: true, pokemon: pokemon});
}

function genSelection(){
  let gen = Math.min(8, Math.max(1, (this.query.gen || '1').parseInt()));
  this.view('index', {gen: gen});
}

function addUser(){
  let info = this.body;
  info['role'] = 'lead';
  EXEC('POST Users --> insert', info, (error, response) => {
    console.log("error", error);
    console.log(response);
    if (error) {
      let errorInfo = error.items[0];
      this.invalid(errorInfo.name, errorInfo.error);
    } else {
      this.success();
    }
  });
}
