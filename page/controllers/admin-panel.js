exports.install = function () {
  ROUTE('+GET /admin-panel/leads', viewLeads);
  ROUTE('+GET /admin-panel/add-admin', 'add-admin');
  ROUTE('+POST /admin-panel/add-admin', addAdmin);
}

function viewLeads() {
  DEF.db.collection('users').aggregate([
    {$match: {role: "lead"}},
    {$sort: {username: 1}},
    {$project: {_id: 0, role: 0}}
  ]).toArray()
    .then(results => {
      let model = {leads: results}
      this.view('admin-panel', model);
    }).catch(err => {
      console.log(err);
      this.invalid('500');
    })
}

function addAdmin(){
  let info = this.body;
  info['role'] = 'admin';
  EXEC('POST Users --> insert', info, (error, response) => {
    if(error){
      let errorInfo = error.items[0];
      this.invalid(errorInfo.name, errorInfo.error);
    } else
      this.success();
  });
}
